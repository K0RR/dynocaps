package com.biom4st3r.dynocaps.register;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynoInventoryDIY;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.items.DynocapHelper;

import net.minecraft.block.DispenserBlock;
import net.minecraft.block.dispenser.DispenserBehavior;
import net.minecraft.block.dispenser.ItemDispenserBehavior;
import net.minecraft.item.ItemStack;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.Direction;

public class DispenserBehaviors {
    static ItemDispenserBehavior DROP_ITEM = new ItemDispenserBehavior();
    public static final DispenserBehavior cap = (pointer, stack) -> {
        IDynocapComponent component = IDynocapComponent.TYPE.get(stack);
        Direction dir = pointer.getBlockState().get(Properties.FACING);
        if (component.isFilled() && dir.ordinal() > 1) {
            if (!ModInit.config.disableDynocapDispensePlacement) {
                component.releaseBlocks(pointer.getWorld(), DynocapHelper.offsetBlockPos(pointer.getBlockPos(), dir, component), null);
            }
        } else if (!component.isFilled() && dir.ordinal() > 1) {
            if (!ModInit.config.disableDynocapDispenseCapture) {
                component.captureBlocks(pointer.getWorld(), DynocapHelper.offsetBlockPos(pointer.getBlockPos(), dir, component), null, true);
            }
        }
        return stack;
    };

    public static final DispenserBehavior capCase = (pointer, stack) -> {
        IDynoInventoryDIY inventory = IDynoInventoryDIY.TYPE.get(stack);
        if (!inventory.isEmpty()) {
            for (int i = 0; i < 4; i++) {
                ItemStack is = inventory.getStack(i);
                if (!is.isEmpty()) {
                    DROP_ITEM.dispense(pointer, inventory.removeStack(i));
                    return stack;
                }
            }
        }
        return stack;
    };

    public static final void init() {
        DispenserBlock.registerBehavior(ItemEnum.Dynocap, cap);
        DispenserBlock.registerBehavior(ItemEnum.CapCase, capCase);
    }
}
