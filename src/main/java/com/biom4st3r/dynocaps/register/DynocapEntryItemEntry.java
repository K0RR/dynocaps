package com.biom4st3r.dynocaps.register;

import java.util.function.Consumer;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntryType;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.registry.Registry;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

public class DynocapEntryItemEntry extends LeafEntry {
    String prefab;

    protected DynocapEntryItemEntry(String prefab, int weight, int quality, LootCondition[] conditions,
            LootFunction[] functions) {
        super(weight, quality, conditions, functions);
        this.prefab = prefab;
    }

    @Override
    protected void generateLoot(Consumer<ItemStack> lootConsumer, LootContext context) {
        lootConsumer.accept(IDynocapComponent.fromPrefab(new Identifier(prefab)));
    }

    public static LootPoolEntryType type;
    public static final void init() {
        type = Registry.register(Registry.LOOT_POOL_ENTRY_TYPE, new Identifier(ModInit.MODID, "rustycap"),
            new LootPoolEntryType(new DynocapEntryItemEntry.Serializer()));
    }

    @Override
    public LootPoolEntryType getType() {
        return type;
    }

    public static LeafEntry.Builder<?> builder(String s) {
        return builder((weight, quan, condi, func) -> {
            return new DynocapEntryItemEntry(s, weight, quan, condi, func);
        });
    }

    public static class Serializer extends LeafEntry.Serializer<DynocapEntryItemEntry> {
        @Override
        public void addEntryFields(JsonObject jsonObject, DynocapEntryItemEntry leafEntry,
                JsonSerializationContext jsonSerializationContext) {
            super.addEntryFields(jsonObject, leafEntry, jsonSerializationContext);
            jsonObject.addProperty("prefabname", leafEntry.prefab);
        }

        @Override
        protected DynocapEntryItemEntry fromJson(JsonObject entryJson, JsonDeserializationContext context, int weight,
                int quality, LootCondition[] conditions, LootFunction[] functions) {
            return new DynocapEntryItemEntry(JsonHelper.getString(entryJson, "prefabname"), weight, quality, conditions, functions);
        }
    }
}
