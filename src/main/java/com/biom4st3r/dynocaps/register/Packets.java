package com.biom4st3r.dynocaps.register;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.DynocapComponent;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.items.ItemTemplate;
import com.biom4st3r.dynocaps.util.ImmutableHashSet;

import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.c2s.play.CustomPayloadC2SPacket;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.util.Identifier;

public final class Packets {
    private static final Identifier
            REQUEST_SETTING_CHANGE = new Identifier(ModInit.MODID, "reqsetchg"),
            SEND_MAX_VALUES = new Identifier(ModInit.MODID, "sndmaxval");

    public static class SERVER {
        public static void init() {
            ServerPlayNetworking.registerGlobalReceiver(REQUEST_SETTING_CHANGE, (server, player, handler, pbb, sender) -> {
                int width = pbb.readInt();
                int height = pbb.readInt();
                int depth = pbb.readInt();
                int color = pbb.readInt();
                boolean placeAir = pbb.readBoolean();
                String name = pbb.readString(25);
                server.execute(() -> {
                    ItemStack stack = player.getMainHandStack();
                    IDynocapComponent component = null;
                    if (ItemEnum.DYNOCAPS.contains(stack.getItem())) {
                        component = (DynocapComponent) IDynocapComponent.TYPE.get(stack);
                    }
                    if (component == null) return;

                    if (!component.getTemplate().isFixedSize()) component.setWidth(Math.min(width, component.getTemplate().max_width));
                    if (!component.getTemplate().isFixedSize()) component.setHeight(Math.min(height, component.getTemplate().max_height));
                    if (!component.getTemplate().isFixedSize()) component.setDepth(Math.min(depth, component.getTemplate().max_depth));

                    if (!component.getTemplate().isFixedColor()) component.setColor(color);
                    component.setPlaceAir(placeAir);
                    component.setName(name);
                });
            });
        }
        public static CustomPayloadS2CPacket sendMaxValues(int width, int height, int depth) {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            ItemTemplate t = ItemTemplate.DEFAULT;
            pbb.writeShort(t.max_width);
            pbb.writeShort(t.max_height);
            pbb.writeShort(t.max_depth);
            pbb.writeByte(t.attributes.size());
            t.attributes.forEach((string) -> pbb.writeString(string));
            return new CustomPayloadS2CPacket(SEND_MAX_VALUES, pbb);
        }
    }

    public static class CLIENT {
        public static void init() {
            ClientPlayNetworking.registerGlobalReceiver(SEND_MAX_VALUES, (client, handler, pbb, sender) -> {
                int maxWidth = pbb.readShort();
                int maxHeight = pbb.readShort();
                int maxDepth = pbb.readShort();
                int size = pbb.readByte();
                ImmutableHashSet<String> attribs = new ImmutableHashSet<>();
                for (int i = 0; i < size; i++) {
                    attribs.add(pbb.readString());
                }

                client.execute(() -> {
                    ItemTemplate.replaceDefaultCap(maxWidth, maxDepth, maxHeight, attribs.build());
                });
            });
        }

        public static CustomPayloadC2SPacket requestSettingsChange(IDynocapComponent component) {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            pbb.writeInt(component.getWidth());
            pbb.writeInt(component.getHeight());
            pbb.writeInt(component.getDepth());
            pbb.writeInt(component.getColor());
            pbb.writeBoolean(component.getPlaceAir());
            pbb.writeString(component.getName(), 25);
            return new CustomPayloadC2SPacket(REQUEST_SETTING_CHANGE, pbb);
        }
    }
}
