package com.biom4st3r.dynocaps.register;

import com.biom4st3r.dynocaps.components.DynocapComponent;
import com.biom4st3r.dynocaps.components.DynocapInventory;
import com.biom4st3r.dynocaps.components.IDynoInventoryDIY;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.items.DynoCapCaseItem;
import com.biom4st3r.dynocaps.items.DynocapItem;

import dev.onyxstudios.cca.api.v3.item.ItemComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.item.ItemComponentInitializer;

public class ComponentRegistry implements ItemComponentInitializer {
    @Override
    public void registerItemComponentFactories(ItemComponentFactoryRegistry registry) {
        registry.registerFor((item) -> item instanceof DynocapItem, IDynocapComponent.TYPE, (stack) -> {
            return new DynocapComponent(((DynocapItem) stack.getItem()).template);
        });
        registry.registerFor((item) -> item instanceof DynoCapCaseItem, IDynoInventoryDIY.TYPE, (stack) -> {
            return new DynocapInventory();
        });
    }
}
