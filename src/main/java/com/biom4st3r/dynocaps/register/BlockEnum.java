package com.biom4st3r.dynocaps.register;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.blocks.DynoSifter;
import com.biom4st3r.dynocaps.blocks.DynocapDisplay;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public enum BlockEnum {
    DISPLAY("display", new DynocapDisplay(FabricBlockSettings.copy(Blocks.STONE))),
    SIFTER("sifter", new DynoSifter()),
    
    ;
    BlockEnum(String id, Block block) {
        this.block = block;
        Registry.register(Registry.BLOCK, new Identifier(ModInit.MODID, id), block);
    }

    private final Block block;

    public Block asBlock() {
        return block;
    }
    public static void loadClass() {
    }
}
