package com.biom4st3r.dynocaps.mixin;

import com.biom4st3r.dynocaps.register.ItemEnum;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.TntEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Util;
import net.minecraft.util.math.Vec3d;

@Mixin(Entity.class)
public abstract class EntityMxn {
    @Inject(
            at = @At(
                value = "INVOKE",
                target = "net/minecraft/entity/Entity.destroy()V",
                ordinal = -1,
                shift = Shift.BEFORE),
            method = "baseTick",
            cancellable = true,
            locals = LocalCapture.NO_CAPTURE)
    public void onItemDeath(CallbackInfo ci) {
        if (!(((Object) this) instanceof ItemEntity)) return;
        ItemEntity theThis = (ItemEntity) (Object) this;
        if (ItemEnum.DYNOCAPS.contains(theThis.getStack().getItem()) && !theThis.world.isClient) {
            ServerWorld world = (ServerWorld) theThis.world;
            if (((ItemEntityAccessor) theThis).getThrower() != null) {
                ServerPlayerEntity player = world.getServer().getPlayerManager().getPlayer(((ItemEntityAccessor) theThis).getThrower());
                player.sendSystemMessage(new LiteralText("<Void> I don't know man. Some weird text?").formatted(Formatting.OBFUSCATED), Util.NIL_UUID);
            }
            TntEntity tnt = new TntEntity(world, theThis.getX(), theThis.getY()+10, theThis.getZ(), null);
            tnt.setCustomNameVisible(true);
            tnt.setCustomName(new LiteralText("Gift from the void"));
            tnt.setFuse(50);
            tnt.noClip = true;
            tnt.setGlowing(true);
            tnt.setVelocity(Vec3d.ZERO);
            tnt.setVelocity(theThis.getVelocity().multiply(-2));
            tnt.setPos(theThis.getX(), theThis.getY()+10, theThis.getZ());
            tnt.setInvulnerable(true);
            world.spawnEntity(tnt);
        }
    }

    @SuppressWarnings("unused")
    private static void init() {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
        cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC | Opcodes.ACC_FINAL, "com/biom4st3r/_genie_/OhBody", null, "Ljava/lang/Object", null);
        MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC, "doTheStuff", "(Lnet/minecraft/world/World;DDDLnet/minecraft/util/math/Vec3d;)V", null, null);
        mv.visitVarInsn(Opcodes.ALOAD, 0);
        mv.visitVarInsn(Opcodes.DLOAD, 1);
        mv.visitVarInsn(Opcodes.DLOAD, 2);
        mv.visitVarInsn(Opcodes.DLOAD, 3);
        mv.visitInsn(Opcodes.ACONST_NULL);
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "net/minecraft/entity/TntEntity", "<init>", "(Lnet/minecraft/world/World;DDDLnet/minecraft/entity/LivingEntity;)V", false);
        mv.visitInsn(Opcodes.ASTORE);
        mv.visitVarInsn(Opcodes.ALOAD, 5);
        mv.visitInsn(Opcodes.ICONST_1);
        mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, "net/minecraft/entity/TntEntity", "setCustomNameVisible", "(Z)V", false);
    }
}
