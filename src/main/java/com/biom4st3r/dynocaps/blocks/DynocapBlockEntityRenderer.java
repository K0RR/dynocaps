package com.biom4st3r.dynocaps.blocks;

import net.minecraft.client.render.LightmapTextureManager;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.world.LightType;

import com.biom4st3r.dynocaps.blocks.DynocapDisplay.DynocapBlockEntity;
import com.biom4st3r.dynocaps.components.IDynocapComponent;

public class DynocapBlockEntityRenderer extends BlockEntityRenderer<DynocapBlockEntity> {
    public DynocapBlockEntityRenderer(BlockEntityRenderDispatcher dispatcher) {
        super(dispatcher);
    }

    @Override
    public void render(DynocapBlockEntity entity, float tickDelta, MatrixStack stack, VertexConsumerProvider vertexConsumers, int badLight, int overlay) {
        if (entity.getDynocap() == ItemStack.EMPTY) return;
        int light = entity.getWorld().getLightLevel(LightType.SKY, entity.getPos().up());

        int realLight = LightmapTextureManager.pack(light, light);
        IDynocapComponent component = IDynocapComponent.TYPE.get(entity.getDynocap());
        if (!entity.getDynocap().isEmpty()) {
            if (component.getCache().isEmpty()) {
                component.getRenderer().fillQuadCache(component.getContainer().asBlockRenderView(), component.getContainer(), component.getCache());
            } else {
                stack.push();
                float maxComponentDim = Math.max(component.getDepth(), Math.max(component.getHeight(), component.getWidth()));
                float scale = 1F/maxComponentDim;

                float xOffset = (-component.getWidth()/2F) * (1F/component.getWidth());
                float zOffset = (-component.getDepth()/2F) * (1F/component.getDepth());
                stack.scale(scale, scale, scale);

                stack.translate(xOffset, 0, zOffset);
                component.getRenderer().renderBlockEntities(tickDelta, stack, vertexConsumers, realLight);
                component.getCache().render(vertexConsumers, stack, realLight, overlay);
                stack.pop();
            }
        }
    }
}
