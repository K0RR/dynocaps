package com.biom4st3r.dynocaps.items;

import java.util.List;

import com.biom4st3r.dynocaps.ModInit;
import com.biom4st3r.dynocaps.components.IDynoInventoryDIY;
import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.guis.capcase.CapCaseGui;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

/**
 * DynoCapCaseItem
 */
public class DynoCapCaseItem extends Item {

    public DynoCapCaseItem() {
        super(new Settings().group(ModInit.DYNO_GROUP).maxCount(1));
    }

    private int findStack(ItemStack is, PlayerInventory pi) {
        for (int i = 0; i < pi.size(); i++) {
            if (pi.getStack(i).equals(is))
                return i;
        }
        return -1;
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        if (world.isClient)
            return TypedActionResult.pass(stack);
        
        user.openHandledScreen(new ExtendedScreenHandlerFactory() {
            @Override
            public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
                return new CapCaseGui(syncId, playerInventory,
                        findStack(user.getStackInHand(hand), playerInventory));
            }

            @Override
            public Text getDisplayName() {
                return new LiteralText("string");
            }

            @Override
            public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf) {
                buf.writeByte(user.inventory.selectedSlot);
            }
        });

        return TypedActionResult.success(stack);
    }

    @Environment(EnvType.CLIENT)
    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        IDynoInventoryDIY inv = IDynoInventoryDIY.TYPE.get(stack);
        for (int i = 0; i < inv.size(); i++) {
            if (inv.getStack(i).isEmpty()) {
                tooltip.add(new TranslatableText("dialog.dynocaps.empty").formatted(Formatting.GRAY));
            } else {
                tooltip.add(new LiteralText(IDynocapComponent.TYPE.get(inv.getStack(i)).getName())
                        .formatted(Formatting.GRAY));
            }
        }
    }

    // @Override
    // public void initComponents(ItemStack arg0, ComponentContainer<CopyableComponent<?>> arg1) {
    //     arg1.put(UniversalComponents.INVENTORY_COMPONENT, new DynoInventory());
    // }
    
}