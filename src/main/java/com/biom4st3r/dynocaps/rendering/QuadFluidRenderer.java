package com.biom4st3r.dynocaps.rendering;

import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.block.FluidRenderer;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.fluid.FluidState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockRenderView;

import com.biom4st3r.dynocaps.ModInitClient;

import org.jetbrains.annotations.Nullable;

import net.fabricmc.fabric.api.renderer.v1.mesh.Mesh;
import net.fabricmc.fabric.api.renderer.v1.mesh.MeshBuilder;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;

public class QuadFluidRenderer extends FluidRenderer {
    BlockPos referencePos;

    public QuadFluidRenderer() {
        this.onResourceReload();
    }

    public boolean render(BlockRenderView world, BlockPos pos, FluidState state) {
        referencePos = pos;
        return this.render(world, pos, null, state);
    }

    @Override
    public boolean render(BlockRenderView world, BlockPos pos, @Nullable VertexConsumer vertexConsumer, FluidState state) {
        return super.render(world, pos, vertexConsumer, state);
    }

    int vertexIndex = 0;
    static final Vector3f NORMAL = new Vector3f(0.0F, 1.0F, 0.0F);
    static final MeshBuilder builder = ModInitClient.getRenderer().meshBuilder();
    static final QuadEmitter emitter = builder.getEmitter();

    @Override
    protected void vertex(VertexConsumer vertexConsumer, double x, double y, double z, float red, float green,
            float blue, float u, float v, int light) {
        emitter
                .pos(vertexIndex, (float) x-referencePos.getX(), (float) y-referencePos.getY(), (float) z-referencePos.getZ())
                .sprite(vertexIndex, 0, u, v)
                .colorIndex(0)
                .lightmap(vertexIndex, light)
                .spriteColor(vertexIndex, 0, RenderHelper.packRGB_I(red, green, blue))
                .normal(vertexIndex, NORMAL);
        vertexIndex++;
        if (vertexIndex > 3) {
            emitter.emit();
            vertexIndex = 0;
        }
    }

    public Mesh build() {
        return builder.build();
    }
}
