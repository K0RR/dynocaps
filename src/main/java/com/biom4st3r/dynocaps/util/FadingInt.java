package com.biom4st3r.dynocaps.util;

public class FadingInt {
    int lowerBound;
    int upperBound;

    boolean down = false;
    int value;

    public FadingInt(int lower, int upper) {
        this.lowerBound = lower;
        this.upperBound = upper;
        this.value = lowerBound;
    }

    public int next(int amount) {
        if (!down) {
            value += amount;
            if (value > upperBound) {
                down = true;
                value -= (value-upperBound);
            }
        } else {
            value -= amount;
            if (value < lowerBound) {
                down = false;
                value += (lowerBound-value);
            }
        }
        return value;
    }

    public int peek() {
        return value;
    }
}
