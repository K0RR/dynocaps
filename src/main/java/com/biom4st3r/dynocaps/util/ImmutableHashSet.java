package com.biom4st3r.dynocaps.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

public class ImmutableHashSet<T> extends HashSet<T> {
    private static final long serialVersionUID = 1L;

    boolean locked = false;

    public HashSet<T> build() {
        this.locked = true;
        return this;
    }

    @Override
    public boolean add(T e) {
        if (locked) throw new IllegalAccessError("HashSet is locked and should not be modified");
        return super.add(e);
    }

    public ImmutableHashSet<T> addThemAll(Set<T> s) {
        this.addAll(s);
        return this;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        if (locked) throw new IllegalAccessError("HashSet is locked and should not be modified");
        return super.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (locked) throw new IllegalAccessError("HashSet is locked and should not be modified");
        return super.removeAll(c);
    }

    @Override
    public boolean remove(Object o) {
        if (locked) throw new IllegalAccessError("HashSet is locked and should not be modified");
        return super.remove(o);
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        if (locked) throw new IllegalAccessError("HashSet is locked and should not be modified");
        return super.removeIf(filter);
    }
}
