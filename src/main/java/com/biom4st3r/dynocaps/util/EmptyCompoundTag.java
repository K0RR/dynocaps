package com.biom4st3r.dynocaps.util;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.nbt.TagReader;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;

public class EmptyCompoundTag extends CompoundTag {
    public static final CompoundTag EMPTY = new EmptyCompoundTag();

    @Override
    public boolean contains(String key) {
        // return super.contains(key);
        return false;
    }

    @Override
    public boolean contains(String key, int type) {
        // return super.contains(key, type);
        return false;
    }

    @Override
    public boolean containsUuid(String key) {
        // return super.containsUuid(key);
        return false;
    }

    @Override
    public CompoundTag copy() {
        return this;
    }

    @Override
    public CompoundTag copyFrom(CompoundTag source) {
        return new CompoundTag().copyFrom(source);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public Tag get(String key) {
        return super.get(key);
    }

    @Override
    public boolean getBoolean(String key) {
        return super.getBoolean(key);
    }

    @Override
    public byte getByte(String key) {
        return super.getByte(key);
    }

    @Override
    public byte[] getByteArray(String key) {
        return super.getByteArray(key);
    }

    @Override
    public CompoundTag getCompound(String key) {
        return super.getCompound(key);
    }

    @Override
    public double getDouble(String key) {
        return super.getDouble(key);
    }

    @Override
    public float getFloat(String key) {
        return super.getFloat(key);
    }

    @Override
    public int getInt(String key) {
        return super.getInt(key);
    }

    @Override
    public int[] getIntArray(String key) {
        return super.getIntArray(key);
    }

    @Override
    public Set<String> getKeys() {
        return Collections.emptySet();
    }

    @Override
    public ListTag getList(String key, int type) {
        return super.getList(key, type);
    }

    @Override
    public long getLong(String key) {
        return super.getLong(key);
    }

    @Override
    public long[] getLongArray(String key) {
        return super.getLongArray(key);
    }

    @Override
    public TagReader<CompoundTag> getReader() {
        return super.getReader();
    }

    @Override
    public short getShort(String key) {
        return super.getShort(key);
    }

    @Override
    public int getSize() {
        // return super.getSize();
        return 0;
    }

    @Override
    public String getString(String key) {
        return super.getString(key);
    }

    @Override
    public byte getType() {
        return super.getType();
    }

    @Override
    public byte getType(String key) {
        // return super.getType(key);
        return 0;
    }

    @Override
    public UUID getUuid(String key) {
        return super.getUuid(key);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public Tag put(String key, Tag tag) {
        // return super.put(key, tag);
        return null;
    }

    @Override
    public void putBoolean(String key, boolean value) {
        // super.putBoolean(key, value);
    }

    @Override
    public void putByte(String key, byte value) {
        // super.putByte(key, value);
    }

    @Override
    public void putByteArray(String key, byte[] value) {
        // super.putByteArray(key, value);
    }

    @Override
    public void putDouble(String key, double value) {
        // super.putDouble(key, value);
    }

    @Override
    public void putFloat(String key, float value) {
        // super.putFloat(key, value);
    }

    @Override
    public void putInt(String key, int value) {
        // super.putInt(key, value);
    }

    @Override
    public void putIntArray(String key, int[] value) {
        // super.putIntArray(key, value);
    }

    @Override
    public void putIntArray(String key, List<Integer> value) {
        // super.putIntArray(key, value);
    }

    @Override
    public void putLong(String key, long value) {
        // super.putLong(key, value);
    }

    @Override
    public void putLongArray(String key, long[] value) {
        // super.putLongArray(key, value);
    }

    @Override
    public void putLongArray(String key, List<Long> value) {
        // super.putLongArray(key, value);
    }

    @Override
    public void putShort(String key, short value) {
        // super.putShort(key, value);
    }

    @Override
    public void putString(String key, String value) {
        // super.putString(key, value);
    }

    @Override
    public void putUuid(String key, UUID value) {
        // super.putUuid(key, value);
    }

    @Override
    public void remove(String key) {
        // super.remove(key);
    }

    @Override
    protected Map<String, Tag> toMap() {
        return Collections.emptyMap();
    }

    @Override
    public String toString() {
        // return super.toString();
        return "{Empty}";
    }

    private static Text TEXT = new LiteralText("{Empty}");
    @Override
    public Text toText(String indent, int depth) {
        // return super.toText(indent, depth);
        return TEXT.copy();
    }

    @Override
    public void write(DataOutput output) throws IOException {
        super.write(output);
    }

    @Override
    public String asString() {
        return "{Empty}";
    }

    @Override
    public Text toText() {
        return TEXT.copy();
    }
}
