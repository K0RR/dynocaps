package com.biom4st3r.dynocaps.dynoworld;

import java.util.List;
import java.util.concurrent.Executor;

import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.WorldGenerationProgressListener;
import net.minecraft.server.world.ServerChunkManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.chunk.WorldChunk;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.Spawner;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.level.ServerWorldProperties;
import net.minecraft.world.level.storage.LevelStorage.Session;

public class DynoServerWorld extends ServerWorld {

    public DynoServerWorld(MinecraftServer server, Executor workerExecutor, Session session,
            ServerWorldProperties properties, RegistryKey<World> registryKey, DimensionType dimensionType,
            WorldGenerationProgressListener worldGenerationProgressListener, ChunkGenerator chunkGenerator,
            boolean debugWorld, long l, List<Spawner> list, boolean bl) {
        super(server, workerExecutor, session, properties, registryKey, dimensionType, worldGenerationProgressListener,
                chunkGenerator, debugWorld, l, list, bl);
    }

    @Override
    public Chunk getChunk(int chunkX, int chunkZ, ChunkStatus leastStatus, boolean create) {
        // TODO Auto-generated method stub
        return super.getChunk(chunkX, chunkZ, leastStatus, create);
    }

    @Override
    public Chunk getChunk(BlockPos pos) {
        return super.getChunk(pos);
    }

    @Override
    public WorldChunk getChunk(int i, int j) {
        return super.getChunk(i, j);
    }

    @Override
    public Chunk getChunk(int chunkX, int chunkZ, ChunkStatus status) {
        return super.getChunk(chunkX, chunkZ, status);
    }

    @Override
    public ServerChunkManager getChunkManager() {
        return super.getChunkManager();
    }

    @Override
    public WorldChunk getWorldChunk(BlockPos pos) {
        return super.getWorldChunk(pos);
    }

    @Override
    public BlockView getExistingChunk(int chunkX, int chunkZ) {
        // TODO Auto-generated method stub
        return super.getExistingChunk(chunkX, chunkZ);
    }

    @Override
    public LongSet getForcedChunks() {
        // TODO Auto-generated method stub
        return super.getForcedChunks();
    }

    @Override
    public BlockPos getRandomPosInChunk(int x, int y, int z, int i) {
        // TODO Auto-generated method stub
        return super.getRandomPosInChunk(x, y, z, i);
    }
    
}
