package com.biom4st3r.dynocaps.dynoworld;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import com.biom4st3r.dynocaps.api.BlockContainer;
import com.mojang.datafixers.DataFixer;
import com.mojang.datafixers.util.Either;

import net.minecraft.entity.Entity;
import net.minecraft.network.Packet;
import net.minecraft.server.WorldGenerationProgressListener;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ChunkHolder.Unloaded;
import net.minecraft.server.world.ChunkTicketType;
import net.minecraft.server.world.ServerChunkManager;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.structure.StructureManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.BlockView;
import net.minecraft.world.PersistentStateManager;
import net.minecraft.world.SpawnHelper;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.source.BiomeArray;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.chunk.UpgradeData;
import net.minecraft.world.chunk.WorldChunk;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.level.storage.LevelStorage.Session;
import net.minecraft.world.poi.PointOfInterestStorage;

public class DynoChunkManager extends ServerChunkManager {
    public DynoChunkManager(ServerWorld serverWorld, Session session, DataFixer dataFixer,
            StructureManager structureManager, Executor workerExecutor, ChunkGenerator chunkGenerator, int viewDistance,
            boolean bl, WorldGenerationProgressListener worldGenerationProgressListener,
            Supplier<PersistentStateManager> supplier) {
        super(serverWorld, session, dataFixer, structureManager, workerExecutor, chunkGenerator, viewDistance, bl,
                worldGenerationProgressListener, supplier);
    }

    public BlockContainer container;
    public int width, height, depth;
    private final int chunkXMax = (int) Math.ceil(width/16F);
    private final int chunkZMax = (int) Math.ceil(depth/16F);

    public void init(int width, int height, int depth, BlockContainer container) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.container = container;
        Registry<Biome> biomes = this.getWorld().getRegistryManager().get(Registry.BIOME_KEY);
        for (int x = 0; x < chunkXMax; x++) {
            for (int z = 0; z < chunkZMax; z++) {
                chunks[x][z] = new DynoChunk(this.getWorld(), new ChunkPos(x, z), new BiomeArray(biomes, new Biome[]{biomes.get(0)}), UpgradeData.NO_UPGRADE_DATA, this.getWorld().getBlockTickScheduler(), this.getWorld().getFluidTickScheduler(), 0, null, null);
            }
        }
    }

    WorldChunk[][] chunks = new WorldChunk[chunkXMax][chunkZMax];

    @Override
    public String getDebugString() {
        return "DynocapChunkManager";
    }

    @Override
    public int getTotalChunksLoadedCount() {
        return (int) (Math.ceil(this.depth * this.width / (16F * 16F)));
    }

    boolean approveChunkCoords(int x, int z) {
        if (x < 0 || z < 0) return false;
        if (x > this.chunkXMax || z > this.chunkZMax) return false;
        return true;
    }

    @Override
    public Chunk getChunk(int x, int z, ChunkStatus leastStatus, boolean create) {
        if (!approveChunkCoords(x, z)) return null;

        return this.chunks[x][z];
    }

    @Override
    public BlockView getChunk(int chunkX, int chunkZ) {
        return this.getChunk(chunkX, chunkZ, ChunkStatus.FULL, false);
    }

    @Override
    public WorldChunk getWorldChunk(int chunkX, int chunkZ) {
        return (WorldChunk) this.getChunk(chunkX, chunkZ);
    }

    @Override
    public WorldChunk getWorldChunk(int chunkX, int chunkZ, boolean create) {
        return (WorldChunk) this.getChunk(chunkX, chunkZ, ChunkStatus.FULL, create);
    }

    @Override
    public CompletableFuture<Either<Chunk, Unloaded>> getChunkFutureSyncOnMainThread(int chunkX, int chunkZ,
            ChunkStatus leastStatus, boolean create) {
        return CompletableFuture.supplyAsync(() -> {
            return Either.left(this.getChunk(chunkX, chunkZ, leastStatus, create));
        });
    }

    @Override
    public boolean isChunkLoaded(int x, int z) {
        return approveChunkCoords(x, z);
    }

    @Override
    public boolean executeQueuedTasks() {
        return true;
    }

    @Override
    public boolean shouldTickEntity(Entity entity) {
        return false;
    }

    public boolean shouldTickChunk(ChunkPos pos) {
        return false;
    }

    @Override
    public boolean shouldTickBlock(BlockPos pos) {
        return false;
    }

    @Override
    public void save(boolean flush) {
        // tick
        // flush
    }

    @Override
    public void close() throws IOException {
        // save
        // close light provider
        // close tacs
    }

    @Override
    public void tick(BooleanSupplier shouldKeepTicking) {
    }

    @Override
    public int getPendingTasks() {
        return -1;
    }

    @Override
    public ChunkGenerator getChunkGenerator() {
        return null;
    }

    @Override
    public int getLoadedChunkCount() {
        return -1;
    }

    @Override
    public void markForUpdate(BlockPos pos) {
    }

    @Override
    public <T> void addTicket(ChunkTicketType<T> ticketType, ChunkPos pos, int radius, T argument) {
    }

    @Override
    public <T> void removeTicket(ChunkTicketType<T> ticketType, ChunkPos pos, int radius, T argument) {
    }

    @Override
    public void setChunkForced(ChunkPos pos, boolean forced) {
    }

    @Override
    public void updateCameraPosition(ServerPlayerEntity player) {
        // TODO?
    }

    @Override
    public void unloadEntity(Entity entity) {
    }

    @Override
    public void loadEntity(Entity entity) {
    }

    @Override
    public void sendToNearbyPlayers(Entity entity, Packet<?> packet) {
    }

    @Override
    public void sendToOtherNearbyPlayers(Entity entity, Packet<?> packet) {
    }

    @Override
    public void applyViewDistance(int watchDistance) {
    }

    @Override
    public void setMobSpawnOptions(boolean spawnMonsters, boolean spawnAnimals) {
    }

    @Override
    public PersistentStateManager getPersistentStateManager() {
        return null;
    }

    @Override
    public PointOfInterestStorage getPointOfInterestStorage() {
        return null;
    }

    @Override
    public SpawnHelper.Info getSpawnInfo() {
        return SpawnHelper.setupSpawn(0, Collections.emptyList(), null);
    }
}
