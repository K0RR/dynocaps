package com.biom4st3r.dynocaps.api;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.jetbrains.annotations.ApiStatus.Experimental;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import net.minecraft.nbt.CompoundTag;

/**
 * A thread-safe reusable Multi-Type list
 * A Object for maintaining multiple resizable arrays of different types.<p>.
 * T represents an interaction medium for the types held in the Multilist.<p>
 * For implemented methods: any time you directly access the interal arrays you MUST either LOCK.lock() or this.borrow().
 * Implemenetations must expose a default constructor(with no arguments) used for cloning.
 */
public abstract class MultiList<T> implements Iterable<T> {

    public static class IntObjectPair<E> {
        public IntObjectPair(){}
        public int index;
        public E e;
        public static <E> IntObjectPair<E> of(int i, E e) {
            IntObjectPair<E> pair = new IntObjectPair<E>();
            pair.index = i;
            pair.e = e;
            return pair;
        }
    }

    /**
     * for array copying without casting to Object[].
     * @param type
     * @param o
     * @param length
     * @return
     */
    protected static Object arrayCopy(Class<?> type, Object o, int length) {
        Object array = Array.newInstance(type, length);
        System.arraycopy(o, 0, array, 0, length);
        return array;
    }

    public static final int DEFAULT_SIZE = 8;

    /**
     * UwU thwed safety.
     */
    protected final ReentrantLock LOCK = new ReentrantLock();
        
    public int loadFactor = DEFAULT_SIZE;

    private Object[] collections;
    
    protected int trueSize = 0;

    private Class<?>[] types;

    public int getSignature() { // no clue if this is good.
        long l = this.trueSize;
        l = Long.rotateLeft(l, 6);
        for(Object o : this.collections) {
            l ^= o.hashCode();
            l = Long.rotateLeft(l, 6);
        }
        return (int) l;
    }

    /**
     * T's will be reused. Do not store references to them. 
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int max = trueSize;
            int index = 0;
            T reusable = newT();

            @Override
            public boolean hasNext() {
                assert_known_size(max);
                return index < max;
            }

            @Override
            public T next() {
                assert_known_size(max);
                reassignT(index++, reusable);
                return reusable;
            }
        };
    }

    public Iterator<IntObjectPair<T>> indexed_iterator() {
        return new Iterator<IntObjectPair<T>>(){
            int max = trueSize;
            IntObjectPair<T> pair = IntObjectPair.of(0, newT());

            @Override
            public boolean hasNext() {
                assert_known_size(max);
                return pair.index < max;
            }
            @Override
            public IntObjectPair<T> next() {
                assert_known_size(max);
                reassignT(pair.index++, pair.e);
                return pair;
            }
        };
    } 

    /**
     * Must be used to determine the real size of any internal array to avoid casting to Object[]
     */
    protected int internal_LengthOfArray(int i) {
        return Array.getLength(this.getCollection(i));
    }

    public MultiList(int defaultSize, Class<?>[] clazzes) {
        this.types = clazzes;
        this.reinit(defaultSize);
    }

    public MultiList(Class<?>[] clazzes) {
        this.types = clazzes;
        this.reinit(DEFAULT_SIZE);
    }
    protected MultiList(int defaultSize, int loadFactor, Class<?>[] clazzes) {
        this.types = clazzes;
        this.reinit(loadFactor);
        this.loadFactor = loadFactor;
    }
    /**
     * Needed for getCopy. Must be implemented, but not used.
     */
    public MultiList() {}

    /**
     * Will return an array of the types of collections this will hold.<p>
     * @return
     */
    protected Class<?>[] getTypes() {
        return types;
    }

    protected final void assert_known_size(int i) {
        if(this.trueSize != i) throw new ConcurrentModificationException("Size of Multilist was changed while iterating");
    }

    /**
     * must be borrowed during iteration
     * @param consumer
     */
    public void forEachEntry(Consumer<T> consumer) {
        int knownSize = this.trueSize;
        for(int i = 0; i < this.length(); i++) {
            assert_known_size(knownSize);
            this.borrowEntry(i, consumer);
        }
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public <R extends MultiList<T>> R getCopy() {
        try {
            MultiList<?> r = this.getClass().newInstance();
            r.collections = new Object[this.collections.length];
            r.types = Arrays.copyOf(this.types, this.types.length);
            for(int i = 0; i < this.getTypes().length; i++) {
                r.collections[i] = arrayCopy(this.getTypes()[i], this.collections[i], this.length());
            }
            r.trueSize = this.trueSize;
            r.clone = this.clone+1;
            return (R) r;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public abstract CompoundTag toTag(CompoundTag tag);
    public abstract void fromTag(CompoundTag tag);

    /**
     * Returns a T with the values held at index i
     * @param <R>
     * @param i
     * @return
     */
    public T get(int i) {
        T t = this.newT();
        this.reassignT(i, t);
        return t;
    }

    private final T borrowable = this.newT();
    /**
     * Suppies a reused reassigned {@code T} to produce less waste than {@link MultiList.get}. Do not retain references.
     * Synchronize to protect {@code this.borrowable}
     * @param t
     */
    public synchronized void borrowEntry(int i, Consumer<? super T> t) {
        this.reassignT(i, borrowable);
        t.accept(borrowable);
    }

    private final T mut = this.newT();

    /**
     * passes T to the consumer, then reassigns the indices to the altered values of T
     * Synchronize to protect {@code this.mut}
     * @param index
     * @param entry
     */
    public synchronized void modifyEntry(int index, Consumer<T> entry) {
        this.reassignT(index, mut);
        entry.accept(mut);
        this.set(index, mut);
    }

    /**
     * Helper method to avoid having to manually cast.
     * @param <R>
     * @param index
     * @return
     */
    @SuppressWarnings({"unchecked"})
    protected final <R> R getCollection(int index) {
        return (R) this.collections[index];
    }
    /**
     * Helper method to avoid having to manually cast.
     * @param <R>
     * @param index
     * @return
     */
    @SuppressWarnings({"unchecked"})
    protected final <R> R getCollection(Class<R> clazz, int index) {
        return (R) this.collections[index];
    }

    /**
     * assigned directly to the internal array array. Intended for Serialization
     * @param <R>
     * @param r
     * @param index
     */
    protected final <R> void internal_copyInto(R r, int index) {
        this.collections[index] = r;
    }

    public boolean isEmpty() {
        return this.length() == 0;
    }

    /**
     * removed null entries and shrinks contained arrays.
     */
    public void trim() {
        int size = this.internal_LengthOfArray(0);
        if(size > this.trueSize) {
            this.resize(this.trueSize - size);
        }
    }

    /**
     * Reinits arrays to default
     */
    public void clear() {
        this.reinit(this.loadFactor);
    }

    public final void borrow(Runnable r) {
        this.LOCK.lock();
        r.run();
        this.LOCK.unlock();
    }

    /**
     * Reassigns contained arrays to {@code size} and resets index
     * 
     * @param size
     */
    public void reinit(int size) {
        // LOCK.lock();
        this.collections = new Object[this.getTypes().length];
        int i = 0;
        for(Class<?> type : this.getTypes()) {
            collections[i] = Array.newInstance(type, size);
            i++;
        }
        this.trueSize = 0;
        // LOCK.unlock();
    }

    /**
     * For internal calling only. intended that you mutate collections inside of Runnable r<p>
     * Any MultiList.add(*) created, but either use this or call checkSize,borrow, and index++ like this.
     * @param r
     */
    protected final void internal_add(Runnable r) {
        this.checkSize();
        borrow(r);
        this.trueSize++;
    }

    public final boolean add(T t) {
        this.checkSize();
        this.set(this.length(), t);
        this.trueSize++;
        return true;
    }

    /**
     * Host must be locked or borrowed while touching the interal arrays
     * @param t
     */
    protected abstract void set(int index,T t);

    /**
     * checks if the array is at max capacity and expands if needed
     */
    protected final void checkSize() {
        if (this.trueSize == this.internal_LengthOfArray(0)) {
            this.resize(this.loadFactor);
        }
    }

    public final T getLatest() {
        return this.get(trueSize-1);
    }

    public void revokeLatest() {
        this.trueSize -= 1;
    }

    protected void resize(int expansion) {
        if(expansion == 0) return;
        int newSize = this.internal_LengthOfArray(0) + expansion;

        for(int i = 0; i < this.getTypes().length; i ++) {
            this.collections[i] = arrayCopy(this.getTypes()[i], this.collections[i], newSize);
        }
    }

    int clone = 0;

    /**
     * returns true if anything was removed
     * @param pred
     * @return
     */
    @Experimental
    @Deprecated
    public final boolean removeIf(Predicate<T> pred) {
        IntList list = new IntArrayList(this.length() / 4); // Random guess of how big the list maybe
        // int[] index = {0};
        for(Iterator<IntObjectPair<T>> i = this.indexed_iterator(); i.hasNext();) {
            IntObjectPair<T> pair = i.next();
            if(pred.test(pair.e)) list.add(pair.index);
        }

        int start = -1;
        int end = -1;
        for(int i : list) { // Search for slices that can be removed and reduce the amount of array copies needed
            if(start == -1 && end == -1) {
                start = i;
                end = i;
                continue;
            }
            if(i == end+1) { // is it sequentally removed
                end = i;
                continue;
            }
            if(start == end) {
                this.remove(start);
            } else {
                this.removeRange(start, end+1);
            }
            start = -1;
            end = -1;
        }

        if(list.size() > 0) return true;
        return false;
    }

    /**
     * returns true if anything was removed
     * @param pred
     * @return
     */
    @Deprecated
    @Experimental
    public final boolean retainIf(Predicate<T> pred) {
        return this.removeIf(pred.negate());
    }

    @Deprecated
    @Experimental
    protected void removeRange(int start, int endExclusive) {
        LOCK.lock();
            for(int i = 0; i < this.getTypes().length; i++) {
                Object array = this.getCollection(i);
                System.arraycopy(array, endExclusive, array, start, this.length()-endExclusive);
            }
        LOCK.unlock();
        trueSize -= (endExclusive-start); 
        this.trim();
    }

    protected abstract T newT();

    /**
     * Must borrow or lock while accessing internal arrays
     * @param i
     * @param t
     */
    protected abstract void reassignT(int i, T t);

    public void remove(int index) {
        LOCK.lock();
            for(int i = 0; i < this.getTypes().length; i++) {
                if(index == this.length()) {
                    this.revokeLatest();
                    continue;
                }
                System.arraycopy(this.getCollection(i),    index+1, this.getCollection(i),    index, this.trueSize-(index+1));
            }
        LOCK.unlock();
        trueSize--;
        this.trim();
    }

    public boolean remove(T t) {
        int i = this.indexOf(t);
        if(i < 0) return false;
        this.remove(i);
        return true;
    }

    public int indexOf(T t) {
        for(Iterator<IntObjectPair<T>> iter = this.indexed_iterator(); iter.hasNext();) {
            IntObjectPair<T> curr = iter.next();
            if(curr.e.equals(t)) return curr.index;
        }
        return -1;
    }

    public int length() {
        return this.trueSize;
    }

    public int size() {
        return this.trueSize;
    }

    @Deprecated
    @Experimental
    public Stream<T> stream() {
        return StreamSupport.stream(new MultiListSplitIterator(0,this.size()), false);
    }

    @Deprecated
    @Experimental
    public class MultiListSplitIterator implements Spliterator<T> {
        private final int startIndex;
        private int endIndex = -1;
        private int index = 0;
        public MultiListSplitIterator(int startIndex, int endIndex) {
            this.startIndex = startIndex;
        }

        @Override
        public boolean tryAdvance(Consumer<? super T> action) {
            if(this.index >= endIndex) return false;
            borrowEntry(index++, action);
            return true;
        }

        @Override
        public Spliterator<T> trySplit() {
            this.endIndex = (endIndex - startIndex) / 2;
            return new MultiListSplitIterator(this.endIndex, endIndex);
        }

        @Override
        public long estimateSize() {
            return this.endIndex-this.startIndex;
        }

        @Override
        public int characteristics() {
            return Spliterator.CONCURRENT | Spliterator.SIZED | Spliterator.NONNULL;
        }
    }
}
