package com.biom4st3r.dynocaps.api;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import com.biom4st3r.dynocaps.mixin.rendering.BlockEntityAccessor;
import com.biom4st3r.dynocaps.util.ClientHelper;
import com.biom4st3r.dynocaps.util.EmptyCompoundTag;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;

import org.apache.commons.lang3.NotImplementedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.nbt.Tag;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.BlockRenderView;
import net.minecraft.world.LightType;
import net.minecraft.world.World;
import net.minecraft.world.chunk.light.LightingProvider;
import net.minecraft.world.level.ColorResolver;

public class BlockContainer implements Iterable<BlockInstance> {
    /**
     *
     */
    private static final String LOOKUP_COMPOUND_STATE = "lbs";

    /**
     *
     */
    private static final String LOOKUP_INT_VAL = "f";

    /**
     *
     */
    private static final String LOOKUP_LIST = "a";

    /**
     * IntArray based on Lookup map.
     */
    private static final String STATES_KEY = "b";

    /**
     * ListTag of CompoundTags.
     */
    private static final String ENTITY_TAGS = "c";

    /**
     * Index when saved.
     */
    private static final String INDEX_KEY = "d";

    /**
     * LongArray.
     */
    private static final String POSITIONS = "e";

    public static int DEFAULT_SIZE = 16;

    /**
     * Because this can be accessed by the MAIN thread and NETTIO thread it must be
     * locked during iteration and reinitilization.
     */
    private ReentrantLock LOCK = new ReentrantLock();

    @NotNull
    public BlockState[] states;

    public long[] poses;

    @NotNull
    public CompoundTag[] beTags;

    @NotNull
    EntityContainer entityContainer;

    private int index = 0;

    public BlockContainer(int defaultSize) {
        this.states = new BlockState[defaultSize];
        this.poses = new long[defaultSize];
        this.beTags = new CompoundTag[defaultSize];
        this.entityContainer = new EntityContainer();
    }

    public EntityContainer getEntities() {
        return FabricLoader.getInstance().isDevelopmentEnvironment() ? this.entityContainer : EmptyEntityContainer.EMPTY;
    }

    public BlockContainer() {
        this(DEFAULT_SIZE);
    }

    private void lock() {
        this.LOCK.lock();
    }

    private void unlock() {
        this.LOCK.unlock();
    }

    public boolean isEmpty() {
        return states.length == 0 || states[0] == null;
    }

    /**
     * removed null entries and shrinks contained arrays.
     * Check back to front until it finds a not null value and resizes down to that point
     */
    public void trim() {
        if (this.states.length > this.index) { // TODO: CHECK THIS
            this.resize(this.index - this.states.length);
        }
        this.view = null;
    }

    /**
     * Reinits arrays to 0.
     */
    public void clear() {
        this.reinit(0);
    }

    private void locker(Runnable r) {
        this.lock();
        r.run();
        this.unlock();
    }

    /**
     * Reassigns contained arrays to {@code size} and resets index.
     *
     * @param size
     */
    public void reinit(int size) {
        this.locker(() -> {
            this.states = new BlockState[size];
            this.poses = new long[size];
            this.beTags = new CompoundTag[size];
            this.getEntities().clear();
            this.view = null;
            this.index = 0;
        });
    }

    /**
     * WARNING: this iterator must be used immediately. It locks the host
     * BlockContainer from thread access until !hasNext()
     */
    @Override
    @Deprecated
    public Iterator<BlockInstance> iterator() {
        return new ContainerIterator();
    }

    /**
     * Uses world to collection all important information from {@code pos}.
     *
     * @param world
     * @param referencePoint
     *                           point offset from the start of the area to be
     *                           contained in this BlockContainer
     * @param pos
     */
    public final void add(BlockRenderView world, Vec3i referencePoint, BlockPos pos) {
        BlockState state = world.getBlockState(pos);
        this.add(state, referencePoint, pos, state.getBlock().hasBlockEntity() ? world.getBlockEntity(pos) : null);
    }

    /**
     * Same as {@code BlockContainer#add(BlockRenderView, Vec3i, BlockPos)}, but you
     * provide the important.
     *
     * @param state
     * @param referencePoint
     * @param pos
     * @param be
     */
    public final void add(BlockState state, Vec3i referencePoint, BlockPos pos, @Nullable BlockEntity be) {
        this._rawAdd(state, pos.subtract(referencePoint), be);
    }

    @VisibleForTesting
    public final void _rawAdd(BlockState state, BlockPos pos, @Nullable BlockEntity be) {
        this._rawAdd(state, pos, be == null ? EmptyCompoundTag.EMPTY : be.toTag(new CompoundTag()));
    }

    public void _rawAdd(BlockState state, BlockPos pos, CompoundTag beTag) {
        lock();
        this.checkSize();
        Preconditions.checkNotNull(state, String.format("Null was passed for state. BlockEntity tag: %s ", beTag));
        this.states[index] = state;
        this.poses[index] = pos.asLong();
        if (beTag == null) beTag = new CompoundTag();
        if (!beTag.isEmpty()) {
            beTag.putInt("x", pos.getX());
            beTag.putInt("y", pos.getY());
            beTag.putInt("z", pos.getZ());
        }
        Preconditions.checkNotNull(beTag, String.format("Null was passed for beTag. Block: %s", state));
        this.beTags[index] = beTag;
        this.index++;
        unlock();
    }

    protected final void checkSize() {
        if (index == states.length) {
            this.resize(DEFAULT_SIZE);
        }
    }

    /**
     * Returns a BlockInstance of the information at index.
     *
     * <p>If you need to get a block at a BlockPos use
     * {@code BlockContainer#asBlockRenderView()}
     *
     * @param i
     * @return
     */
    @Nullable
    public BlockInstance get(int i) {
        if (i >= index) {
            return null;
        }
        return new BlockInstance(this.states[i], BlockPos.fromLong(this.poses[i]), beTags[i]);
    }

    public BlockInstance getLatest() {
        return this.get(index-1);
    }

    public void revokeLatest() {
        this.index -= 1;
    }
    
    public final void addEmpty(BlockPos refPoint, BlockPos pos) {
        this.add(Blocks.AIR.getDefaultState(), refPoint, pos, null);
    }

    protected void resize(int expansion) {
        int newSize = states.length;
        states = Arrays.copyOf(states, newSize);
        poses = Arrays.copyOf(poses, newSize);
        beTags = Arrays.copyOf(beTags, newSize);
    }

    public BlockContainer getCopy() {
        BlockContainer c = new BlockContainer();
        c.states = Arrays.copyOf(this.states, this.states.length);
        c.poses = Arrays.copyOf(this.poses, this.poses.length);
        c.beTags = Arrays.copyOf(this.beTags, this.beTags.length);
        c.index = this.index;
        c.clone = this.clone + 1;
        return c;
    }

    int clone = 0;

    /**
     * Alt option to using {@code BlockContainer#iterator()}.
     *
     * @param consumer
     * @param world
     */
    public void forEach(Consumer<BlockInstance> consumer, World world) {
        BlockInstance instance = new BlockInstance();
        BlockPos.Mutable pos = new BlockPos.Mutable();
        this.locker(() -> {
            for (int i = 0; i < index; i++) {
                BlockState state = states[i];
                pos.set(poses[i]);
                instance.reassign(state, pos, beTags[i]);
                consumer.accept(instance);
            }
        });
    }

    public void forEachReversed(Consumer<BlockInstance> consumer) {
        BlockInstance instance = new BlockInstance();
        BlockPos.Mutable pos = new BlockPos.Mutable();
        this.locker(() -> {
            for (int i = index-1; i > -1; i--) {
                BlockState state = states[i];
                pos.set(poses[i]);
                instance.reassign(state, pos, beTags[i]);
                consumer.accept(instance);
            }
        });
    }

    /**
     * Creates BlockEntity from information at index i.
     *
     * @param i
     * @return
     */
    @Nullable
    @Environment(EnvType.CLIENT)
    public BlockEntity constructBlockEntity(int i) {
        if (i == -1 || beTags[i].isEmpty()) {
            return null;
        } else {
            BlockEntity be;
            try {
                be = BlockEntity.createFromTag(states[i], beTags[i]);
            } catch(Throwable t) {
                t.printStackTrace();
                return null;
            }
            ((BlockEntityAccessor) be).setCachedState(states[i]);
            ((BlockEntityAccessor) be).setWorld(ClientHelper.world.get());
            return be;
        }
    }

    public CompoundTag toTag(CompoundTag tag) {
        if (this.isEmpty()) {
            tag.putByte("empty", (byte) 1);
            return tag;
        }
        this.trim();
        tag.putLongArray(POSITIONS, this.poses);
        tag.putInt(INDEX_KEY, this.index);
        ListTag etags = new ListTag();
        for (CompoundTag etag : beTags) {
            etags.add(etag);
        }
        tag.put(ENTITY_TAGS, etags);
        // Building lookup map
        Object2IntOpenHashMap<BlockState> lookupMap = new Object2IntOpenHashMap<>();
        int lookupi = 0;
        for (BlockState state : states) {
            int i = lookupMap.getOrDefault(state, -1);
            if (i == -1) {
                lookupMap.put(state, lookupi);
                lookupi++;
            }
        }
        IntList blockstates = new IntArrayList(states.length);
        for (BlockState state : states) {
            blockstates.add(lookupMap.getInt(state));
        }
        tag.putIntArray(STATES_KEY, blockstates.toIntArray());

        ListTag lookupList = new ListTag();
        for (Object2IntMap.Entry<BlockState> entry : lookupMap.object2IntEntrySet()) {
            CompoundTag ct = new CompoundTag();
            ct.putInt(LOOKUP_INT_VAL, entry.getIntValue());
            ct.put(LOOKUP_COMPOUND_STATE, serializeBlockState(entry.getKey()));
            lookupList.add(ct);
        }
        tag.put(LOOKUP_LIST, lookupList);

        this.getEntities().toTag(tag);

        return tag;
    }

    static class DynocapSerializationException extends RuntimeException {
        private static final long serialVersionUID = 2515395683077980628L;
        DynocapSerializationException(String str) {
            super(str);
        }
    }

    public static Tag serializeBlockState(BlockState state) {
        Tag tag = BlockState.CODEC.stable().encodeStart(NbtOps.INSTANCE, state).getOrThrow(true, (str) -> new DynocapSerializationException(str));
        return tag;
    }

    public static BlockState deserializeBlockState(Tag tag) {
        return BlockState.CODEC.stable().decode(NbtOps.INSTANCE, tag).getOrThrow(true, (str) -> new DynocapSerializationException(str)).getFirst();
    }

    public void fromTag(CompoundTag tag) {
        this.clear();
        if (tag.getByte("empty") == 1) return;
        this.reinit(tag.getInt(INDEX_KEY));
        this.index = tag.getInt(INDEX_KEY);
        this.poses = tag.getLongArray(POSITIONS);
        ListTag entityTags = (ListTag) tag.get(ENTITY_TAGS);
        for (int i = 0; i < entityTags.size(); i++) {
            this.beTags[i] = entityTags.getCompound(i);
        }
        Int2ObjectMap<BlockState> lookupmap = new Int2ObjectOpenHashMap<>(10);
        ListTag lookupList = (ListTag) tag.get(LOOKUP_LIST);
        lookupList.forEach((t) -> {
            CompoundTag ct = (CompoundTag) t;
            lookupmap.put(ct.getInt(LOOKUP_INT_VAL), deserializeBlockState(ct.get(LOOKUP_COMPOUND_STATE)));
        });
        int[] statearray = tag.getIntArray(STATES_KEY);
        for (int i = 0; i < statearray.length; i++) {
            this.states[i] = lookupmap.get(statearray[i]);
        }
        this.getEntities().fromTag(tag);
    }

    public int getSize() {
        return this.index;
    }
    public int getLength() {
        return this.index;
    }
    public int length() {
        return this.index;
    }

    public static BlockContainer fromTagStatic(CompoundTag tag) {
        BlockContainer container = new BlockContainer(0);
        container.fromTag(tag);
        return container;
    }

    // @Environment(EnvType.CLIENT)
    private class ContainerBlockRenderView implements BlockRenderView {
        Long2IntOpenHashMap blockPosToInstanceMap;
        ContainerBlockRenderView(int size) {
            blockPosToInstanceMap = new Long2IntOpenHashMap(size);
            for (int i = 0; i < index; i++) {
                blockPosToInstanceMap.put(poses[i], i);
            }
        }

        @Override
        public int getBaseLightLevel(BlockPos pos, int ambientDarkness) {
            return 12;
        }

        @Override
        public int getLightLevel(LightType type, BlockPos pos) {
            return 12;
        }

        @Override
        public int getMaxLightLevel() {
            return 12;
        }

        @Override
        public BlockEntity getBlockEntity(BlockPos pos) {
            return constructBlockEntity(blockPosToInstanceMap.getOrDefault(pos.asLong(), -1));
        }

        @Override
        public BlockState getBlockState(BlockPos pos) {
            int i = blockPosToInstanceMap.getOrDefault(pos.asLong(), -1);
            return i == -1 ? Blocks.AIR.getDefaultState() : states[i];
        }

        @Override
        public FluidState getFluidState(BlockPos pos) {
            return getBlockState(pos).getFluidState();
        }

        @Override
        public float getBrightness(Direction direction, boolean shaded) {
            return 1.0F;
        }

        @Override
        public LightingProvider getLightingProvider() {
            throw new NotImplementedException("lighting provider is not used");
        }

        @Override
        public int getColor(BlockPos pos, ColorResolver colorResolver) {
            ClientPlayerEntity player = ClientHelper.player.get();
            return colorResolver.getColor(player.clientWorld.getBiome(player.getBlockPos()), player.getX(), player.getZ());
        }
    }

    private class ContainerIterator implements Iterator<BlockInstance> {
        int i = 0;
        private ContainerIterator() {
            lock();
        }

        @Override
        public boolean hasNext() {
            boolean hasNext = !(i == index) && !isEmpty();
            if (!hasNext) unlock();
            return hasNext;
        }

        BlockInstance instance = new BlockInstance();
        BlockPos.Mutable pos = new BlockPos.Mutable();

        @Override
        public BlockInstance next() {
            instance.reassign(states[i], pos.set(poses[i]), beTags[i]);
            i++;
            return instance;
        }
    }

    @Environment(EnvType.CLIENT)
    ContainerBlockRenderView view;

    @Environment(EnvType.CLIENT)
    public BlockRenderView asBlockRenderView() {
        return view == null ? view = new ContainerBlockRenderView(index) : view;
    }

    public ProtoWorld asModifiableWorld(int width, int height, int depth) {
        return null;
    }

    public int getHashCode() {
        return Arrays.hashCode(this.states);
    }
}
