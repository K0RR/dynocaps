package com.biom4st3r.dynocaps.api;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.math.BlockPos;

import org.jetbrains.annotations.Nullable;

public class BlockInstance implements Cloneable {
    public BlockState state;
    public BlockPos relativePos;
    @Nullable
    public CompoundTag entityTag;
    public static final BlockInstance EMPTY = new BlockInstance(Blocks.AIR.getDefaultState(), BlockPos.ORIGIN, null);

    public BlockInstance() {
    }

    public BlockInstance(BlockState state, BlockPos pos, CompoundTag entityTag) {
        this.state = state;
        this.relativePos = pos;
        this.entityTag = entityTag;
    }

    protected void reassign(BlockState state, BlockPos pos, CompoundTag entityTag) {
        this.state = state;
        this.relativePos = pos;
        this.entityTag = entityTag;
    }

    public void reassign(BlockState state, long l, CompoundTag tag) {
        this.state = state;
        this.relativePos = BlockPos.fromLong(l);
        this.entityTag = tag;
    }

    public boolean isFluid() {
        return !this.state.getFluidState().isEmpty();
    }
    public boolean isBlockEntity() {
        return state.getBlock().hasBlockEntity();
    }

    /**
     * Returns a static instance of BlockInstance with data that doesn't magically change.
     */
    @Override
    public BlockInstance clone() {
        BlockInstance instance = new BlockInstance();
        instance.state = this.state;
        instance.relativePos = this.relativePos.toImmutable();
        instance.entityTag = this.entityTag;
        return instance;
    }
}
