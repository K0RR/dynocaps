package com.biom4st3r.dynocaps.api;

import java.util.Map;

import com.biom4st3r.dynocaps.components.IDynoInventoryDIY;
import com.biom4st3r.dynocaps.register.ItemEnum;
import com.google.common.collect.Maps;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Util;

public interface UnBookBan {
    UnBookBan DEFAULT = (spe, index, inventory, is) -> {
        inventory.setStack(index, new ItemStack(is.getItem()));
    };

    Map<ItemConvertible, UnBookBan> unBookBanActions = Util.make(() -> {
        Map<ItemConvertible, UnBookBan> map = Maps.newHashMap();
        map.put(ItemEnum.CapCase, (spe, index, inv, stack) -> {
            IDynoInventoryDIY inventory = IDynoInventoryDIY.TYPE.get(stack);
            for (int i = 0; i < 4; i++) {
                ItemStack is = inventory.getStack(i);
                if (is.isEmpty()) continue;
                if (inv.insertStack(is)) continue;
                if (spe.getEnderChestInventory().canInsert(is)) {
                    spe.getEnderChestInventory().addStack(is);
                }
                inventory.setStack(0, ItemStack.EMPTY);
            }
        });

        return map;
    });

    static UnBookBan get(ItemConvertible item) {
        return unBookBanActions.getOrDefault(item, DEFAULT);
    }

    void fix(ServerPlayerEntity spe, int inventoryIndex, PlayerInventory inventory, ItemStack is);
}
