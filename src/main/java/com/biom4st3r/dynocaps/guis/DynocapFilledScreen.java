package com.biom4st3r.dynocaps.guis;

import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Quaternion;
import net.minecraft.util.profiler.Profiler;

import net.fabricmc.fabric.impl.client.indigo.renderer.render.BlockRenderContext;

import com.biom4st3r.dynocaps.components.IDynocapComponent;
import com.biom4st3r.dynocaps.rendering.AgnosticRenderContext;
import com.biom4st3r.dynocaps.rendering.DynocapRenderer;
import com.biom4st3r.dynocaps.util.ClientHelper;
import com.mojang.blaze3d.systems.RenderSystem;
import io.github.cottonmc.cotton.gui.GuiDescription;

public class DynocapFilledScreen extends NoPauseCottonScreen {
    AgnosticRenderContext cache;
    DynocapRenderer renderer;

    public DynocapFilledScreen(GuiDescription description) {
        super(description);
        ItemStack stack = ClientHelper.player.get().getMainHandStack();
        this.component = IDynocapComponent.TYPE.get(stack);
        this.cache = component.getCache();
        this.renderer = component.getRenderer();
        this.cache.clear();
        this.renderer.clear();
        renderer.fillQuadCache(component);
    }

    public IDynocapComponent component;

    public BlockRenderContext context = new BlockRenderContext();

    public static Profiler profiler = ClientHelper.client.getProfiler();

    @Override
    public void init() {
        super.init();
    }

    @Override
    public void render(MatrixStack stack, int mouseX, int mouseY, float delta) {
        super.render(stack, mouseX, mouseY, delta);
        profiler.swap("dynocap_gui");
        renderBlock(this.getXMid()+55, this.getYMid()-5, delta);
    }

    public static Quaternion X_ROTATION = new Quaternion(Vector3f.POSITIVE_X, 180+25, true);

    /**
     * Local stack for {@link #DynocapFilledScreen}.
     */
    public static MatrixStack stack = new MatrixStack();

    public void renderBlock(float x, float y, float delta) {
        int maxComponentDim = Math.max(component.getDepth(), Math.max(component.getHeight(), component.getWidth()));
        float scale = (float) Math.pow(10F/maxComponentDim, 1.8) * maxComponentDim;
        //CHECKSTYLE.OFF: IndentationCheck
        RenderSystem.pushMatrix();
            RenderSystem.translatef(x, y, 500); // Honestly just copied from PlayerInventoryScreen
            RenderSystem.scalef(1, 1, -1f); //    👆
            RenderSystem.color4f(1, 1, 1, 1);
            // Settings up my stack
            stack.push();
                // translates to where the center of the model will be so that rotation is from the middle
                stack.translate((component.getWidth()/2.0D)-component.getWidth()/4.7D, component.getHeight()/2.0D-component.getHeight()/3, component.getDepth()/2.0D);
                stack.scale(scale, scale, scale);
                stack.multiply(X_ROTATION);
                stack.multiply(new Quaternion(Vector3f.POSITIVE_Y, (float) ((System.currentTimeMillis()/25.0D) % 359.0D), true));
                // rotation is done so reset time
                stack.translate(-component.getWidth()/2.0D, -component.getHeight()/2.0D, -component.getDepth()/2.0D);
                int light = 0x00800080;
                // VertexConsumerProvider.Immediate immediate = ClientHelper.client.getBufferBuilders().getEntityVertexConsumers();
                renderer.renderBlockEntities(delta, stack, AgnosticRenderContext.IMMEDIATE, light);
                cache.render(AgnosticRenderContext.IMMEDIATE, stack, light, OverlayTexture.DEFAULT_UV);
                component.getEntities().render(delta, stack, AgnosticRenderContext.IMMEDIATE, light);

                AgnosticRenderContext.IMMEDIATE.draw();
            stack.pop();
        RenderSystem.popMatrix();
        //CHECKSTYLE.ON: IndentationCheck
    }

    // public void consumeTime()
    // {
    //     //CHECKSTYLE.OFF: IndentationCheck, WhitespaceAfterCheck, LeftCurlyCheck
    //           for(;;) { for(;;) { for(;;) {
    //         for(;;) {                for(;;) {
    //     for(;;) {                          for(;;) {
    //   for(;;) {                               for(;;) {
    // for(;;) {                                    for(;;) {
    // for(;;) {             /*Hello*/              for(;;) {
    // for(;;) {                                    for(;;) {
    //   for(;;) {                               for(;;) {
    //     for(;;) {                          for(;;) {
    //         for(;;) {                for(;;) {
    //                }}}}}}}}}}}}}}}}}}}}}
    //     //CHECKSTYLE.ON:
    // }

    public int getXMid() {
        return this.width/2;
    }

    public int getYMid() {
        return this.height/2;
    }

    @Override
    public void onClose() {
        this.component.getCache().clear();
        this.component.getRenderer().clear();
        super.onClose();
    }
}
