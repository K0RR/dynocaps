
[![](https://hitcounter.pythonanywhere.com/count/tag.svg?url=https%3A%2F%2Fgitlab.com%2Fbiom4st3r%2Fdynocaps)]()
[![](https://jitpack.io/v/com.gitlab.biom4st3r/dynocaps.svg)](https://jitpack.io/#com.gitlab.biom4st3r/dynocaps)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Generic badge](https://img.shields.io/badge/Maintained-sometimes-yellow.svg)](https://ko-fi.com/K3K033W6X)
[![Generic badge](https://img.shields.io/badge/Made_with-Java-red.svg)](https://ko-fi.com/K3K033W6X/)
[![Generic badge](https://img.shields.io/badge/Powered_by-Pizza-orange.svg)](https://ko-fi.com/K3K033W6X)
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/K3K033W6X)
# DynoCaps
![Imgur](https://i.imgur.com/ZpKJjlI.png)

## About
Dynocaps let you explore the world from the comfort of your own house. It's always a pain in the $$@ when you decide you want to up and find a new place to live, but now you can just shove you house and valuable in a Dynocap and off you go.

## Notables
* Anviless renaming
* When holding your Dynocap the affected area is outlined
* 16,777,216 color support to help you sort your caps
* Convenient carrying case that hold up to 4 caps and displays which ones are inside
* If you can't remember why you named your cap ```Weird thing in woods 7```, no worries! A preview is avalible.
* Can be saved and added to loottables
* Define custom Dynocaps via datapack(Check out the `templates` folder)

## Issues
If you find an issue please report it at my [Gitlab](https://gitlab.com/biom4st3r/dynocaps/issues).

Feel free to request new features also.

```If your issue is related to a crash: please provide a crash log link hosted on pastebin/hastebin/etc```

[Pastebin](https://pastebin.com)

[Hastebin](https://hastebin.com)

[Datapack Features](https://gitlab.com/biom4st3r/dynocaps/-/blob/default/FOR_MOD_PACK_MAKERS.md)


## FAQ
* Will you forge? No

## License
[GNU General Public License 3](https://gitlab.com/biom4st3r/dynocaps/-/blob/master/LICENSE)